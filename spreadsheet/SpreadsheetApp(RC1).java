import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class SpreadsheetApp {

    private static JLabel resultLabel;
    private static JTable table;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(SpreadsheetApp::createAndShowGUI);
    }

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Professional Spreadsheet");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 700);

        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return true;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return Double.class;
            }
        };

        for (int i = 0; i < 26; i++) {
            model.addColumn(Character.toString((char) ('A' + i)));
        }

        for (int i = 0; i < 100; i++) {
            model.addRow(new Object[26]);
        }

        table = new JTable(model);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setCellSelectionEnabled(true);
        table.setFont(new Font("Arial", Font.PLAIN, 14));
        table.setRowHeight(25);

        // Кастомний рендерер для комірок
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value,
                                                           boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (isSelected) {
                    c.setBackground(new Color(184, 207, 229));
                    c.setForeground(Color.BLACK);
                } else {
                    c.setBackground(row % 2 == 0 ? Color.WHITE : new Color(240, 240, 240));
                    c.setForeground(Color.BLACK);
                }
                setBorder(BorderFactory.createCompoundBorder(getBorder(),
                        BorderFactory.createEmptyBorder(0, 5, 0, 5)));
                return c;
            }
        };
        cellRenderer.setHorizontalAlignment(JLabel.RIGHT);
        table.setDefaultRenderer(Object.class, cellRenderer);

        // Кастомний рендерер для заголовків
        JTableHeader header = table.getTableHeader();
        header.setDefaultRenderer(new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value,
                                                           boolean isSelected, boolean hasFocus, int row, int column) {
                JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                label.setBackground(new Color(66, 139, 202));
                label.setForeground(Color.WHITE);
                label.setFont(label.getFont().deriveFont(Font.BOLD));
                label.setHorizontalAlignment(JLabel.CENTER);
                return label;
            }
        });
        header.setReorderingAllowed(false);

        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);

        JTable rowTable = new RowNumberTable(table);
        scrollPane.setRowHeaderView(rowTable);
        scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, rowTable.getTableHeader());

        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
        frame.add(bottomPanel, BorderLayout.SOUTH);

        JButton sumButton = new JButton("Sum");
        sumButton.setFont(new Font("Arial", Font.BOLD, 14));
        sumButton.setBackground(new Color(92, 184, 92));
        sumButton.setForeground(Color.WHITE);
        sumButton.setFocusPainted(false);
        sumButton.addActionListener((ActionEvent e) -> calculateSum());

        JPanel toolbar = new JPanel(new FlowLayout(FlowLayout.LEFT));
        toolbar.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
        frame.add(toolbar, BorderLayout.NORTH);
        toolbar.add(sumButton);

        resultLabel = new JLabel("Result: ");
        resultLabel.setFont(new Font("Arial", Font.PLAIN, 14));
        bottomPanel.add(resultLabel);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private static void calculateSum() {
        int[] selectedRows = table.getSelectedRows();
        int[] selectedColumns = table.getSelectedColumns();

        if (selectedRows.length == 0 || selectedColumns.length == 0) {
            resultLabel.setText("Результат: Виберіть комірки");
            return;
        }

        double sum = 0;
        List<Point> selectedCells = new ArrayList<>();

        for (int row : selectedRows) {
            for (int column : selectedColumns) {
                Object value = table.getValueAt(row, column);
                if (value instanceof Number) {
                    sum += ((Number) value).doubleValue();
                    selectedCells.add(new Point(column, row)); // Зберігаємо координати комірок
                }
            }
        }

        // Визначаємо місце вставки результату сумації
        if (!selectedCells.isEmpty()) {
            Point lastCell = selectedCells.get(selectedCells.size() - 1);
            int insertColumn = lastCell.x + 1;
            if (insertColumn < table.getColumnCount()) {
                table.setValueAt(sum, lastCell.y, insertColumn); // Вставка результату поруч з виділеними комірками
            } else {
                resultLabel.setText("Немає місця для вставки результату суми");
                return;
            }
        }

        resultLabel.setText("Результат: " + sum);
    }

    /**
     * Внутрішній клас для створення таблиці з номерами рядків
     */
    private static class RowNumberTable extends JTable {
        private JTable mainTable;

        public RowNumberTable(JTable table) {
            mainTable = table;
            setModel(new DefaultTableModel());
            addColumn(new TableColumn());
            getColumnModel().getColumn(0).setHeaderValue("№");
            getTableHeader().setReorderingAllowed(false);
            setPreferredScrollableViewportSize(new Dimension(50, 0));
            setRowHeight(mainTable.getRowHeight());
            setBackground(Color.LIGHT_GRAY);
            setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }

        @Override
        public int getRowCount() {
            return mainTable.getRowCount();
        }

        @Override
        public Object getValueAt(int row, int column) {
            return row + 1; // Повертає номер рядка
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }
}
//claude-3-5-sonnet-20240620
