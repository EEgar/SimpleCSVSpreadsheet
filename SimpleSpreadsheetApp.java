import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleSpreadsheetApp extends JFrame {
    private JTable table;
    private DefaultTableModel model;
    private JLabel sumLabel;
    private JTextField formulaBar;

    public SimpleSpreadsheetApp() {
        setTitle("Enhanced Spreadsheet");
        setSize(1000, 700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        // Create table
        model = new DefaultTableModel(20, 9);  // Changed to 9 columns (A to I)
        table = new JTable(model) {
            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component comp = super.prepareRenderer(renderer, row, column);
                if (isCellSelected(row, column)) {
                    comp.setBackground(new Color(173, 216, 230)); // Light blue for selected cell
                } else {
                    comp.setBackground(Color.WHITE);
                }
                return comp;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return column != 0; // Make the first column (row numbers) non-editable
            }
        };
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION); // Allow multiple selection
        table.setCellSelectionEnabled(true);
        table.setFont(new Font("Arial", Font.PLAIN, 14));

        // Customize table header
        JTableHeader header = table.getTableHeader();
        header.setBackground(new Color(70, 130, 180)); // Steel blue
        header.setForeground(Color.WHITE);
        header.setFont(new Font("Arial", Font.BOLD, 14));

        // Set column names (A, B, C, ...) starting from the second column
        for (int i = 0; i < table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setHeaderValue(i == 0 ? "" : String.valueOf((char) ('A' + i - 1)));
        }

        // Add row numbers
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.getColumnModel().getColumn(0).setPreferredWidth(40);
        table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (column == 0) {
                    setText(String.valueOf(row + 1));
                    setBackground(new Color(240, 240, 240));
                    setHorizontalAlignment(JLabel.CENTER);
                } else {
                    setHorizontalAlignment(JLabel.LEFT);
                }
                return c;
            }
        });

        // Create toolbar
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.setBackground(new Color(240, 240, 240));

        JButton sumButton = createStyledButton("Sum Selected", new Color(46, 139, 87));
        sumButton.addActionListener(e -> sumSelectedCells());
        toolBar.add(sumButton);

        JButton saveButton = createStyledButton("Save", new Color(65, 105, 225));
        saveButton.addActionListener(e -> saveCSV());
        toolBar.add(saveButton);

        JButton openButton = createStyledButton("Open", new Color(205, 133, 63));
        openButton.addActionListener(e -> openCSV());
        toolBar.add(openButton);

        JButton clearButton = createStyledButton("Clear All", new Color(220, 20, 60));
        clearButton.addActionListener(e -> clearSpreadsheet());
        toolBar.add(clearButton);

        // Create formula bar
        formulaBar = new JTextField();
        formulaBar.setFont(new Font("Arial", Font.PLAIN, 14));
        
        // Create sum label
        sumLabel = new JLabel("Sum: 0.00");
        sumLabel.setFont(new Font("Arial", Font.BOLD, 14));
        sumLabel.setForeground(new Color(0, 100, 0));

        // Layout components
        setLayout(new BorderLayout());
        add(toolBar, BorderLayout.NORTH);
        add(new JScrollPane(table), BorderLayout.CENTER);
        
        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.add(new JLabel("Formula: "), BorderLayout.WEST);
        bottomPanel.add(formulaBar, BorderLayout.CENTER);
        bottomPanel.add(sumLabel, BorderLayout.EAST);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    private JButton createStyledButton(String text, Color color) {
        JButton button = new JButton(text);
        button.setBackground(color);
        button.setForeground(Color.WHITE);
        button.setFocusPainted(false);
        button.setFont(new Font("Arial", Font.BOLD, 12));
        return button;
    }

    private void sumSelectedCells() {
        double sum = calculateSumSelected();
        int[] selectedRows = table.getSelectedRows();
        int[] selectedCols = table.getSelectedColumns();
        
        if (selectedRows.length > 0 && selectedCols.length > 0) {
            int lastRow = selectedRows[selectedRows.length - 1];
            int lastCol = selectedCols[selectedCols.length - 1];
            
            // Find the next empty cell below the selection
            int resultRow = lastRow + 1;
            while (resultRow < table.getRowCount() && table.getValueAt(resultRow, lastCol) != null 
                   && !table.getValueAt(resultRow, lastCol).toString().isEmpty()) {
                resultRow++;
            }
            
            if (resultRow < table.getRowCount()) {
                table.setValueAt(String.format("%.2f", sum), resultRow, lastCol);
                table.changeSelection(resultRow, lastCol, false, false);
            } else {
                JOptionPane.showMessageDialog(this, "No empty cell found below the selection to display the sum.");
            }
        }
        
        sumLabel.setText(String.format("Sum: %.2f", sum));
    }

    private double calculateSumSelected() {
        int[] selectedRows = table.getSelectedRows();
        int[] selectedCols = table.getSelectedColumns();
        double sum = 0.0;

        for (int row : selectedRows) {
            for (int col : selectedCols) {
                Object value = table.getValueAt(row, col);
                if (value != null) {
                    try {
                        sum += Double.parseDouble(value.toString());
                    } catch (NumberFormatException ignored) {
                    }
                }
            }
        }

        return sum;
    }

private void saveCSV() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try (CSVPrinter printer = new CSVPrinter(new FileWriter(file), CSVFormat.DEFAULT)) {
                for (int i = 0; i < model.getRowCount(); i++) {
                    List<String> row = new ArrayList<>();
                    for (int j = 1; j < model.getColumnCount(); j++) {  // Start from 1 to skip row numbers
                        Object value = model.getValueAt(i, j);
                        row.add(value != null ? value.toString() : "");
                    }
                    printer.printRecord(row);
                }
                JOptionPane.showMessageDialog(this, "File saved successfully!");
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "Error saving file: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void openCSV() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try (Reader reader = new FileReader(file)) {
                Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(reader);
                model.setRowCount(0);
                for (CSVRecord record : records) {
                    Object[] row = new Object[record.size() + 1];  // +1 for row number
                    row[0] = model.getRowCount() + 1;  // Set row number
                    for (int i = 0; i < record.size(); i++) {
                        row[i + 1] = record.get(i);
                    }
                    model.addRow(row);
                }
                JOptionPane.showMessageDialog(this, "File loaded successfully!");
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "Error loading file: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void clearSpreadsheet() {
        int confirm = JOptionPane.showConfirmDialog(this, 
            "Are you sure you want to clear all data?", 
            "Confirm Clear", JOptionPane.YES_NO_OPTION);
        if (confirm == JOptionPane.YES_OPTION) {
            for (int i = 0; i < model.getRowCount(); i++) {
                for (int j = 0; j < model.getColumnCount(); j++) {
                    model.setValueAt(null, i, j);
                }
            }
            sumLabel.setText("Sum: 0.00");
            formulaBar.setText("");
        }
    }

  public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        SwingUtilities.invokeLater(() -> {
            SimpleSpreadsheetApp app = new SimpleSpreadsheetApp();
            app.setVisible(true);
        });
    }
}
